//
//  ChallengeConcreteUITests.swift
//  ChallengeConcreteUITests
//
//  Created by Emannuel Carvalho on 17/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import XCTest

import Embassy
import EnvoyAmbassador

class ChallengeConcreteUITests: UITestBase {
    
    func testShowingRepos() {
        router["/search/repositories"] = DelayResponse(JSONResponse(handler: { [unowned self] _ -> Any in
            return self.mockReposData
        }), delay: .delay(seconds: 0.0))
        
        app.launch()

        XCTAssert(
            XCUIApplication()
                .collectionViews
                .cells
                .staticTexts["JavaRepo"]
                .exists
        )
        
        XCTAssert(
            XCUIApplication()
                .collectionViews
                .cells
                .staticTexts["A very creative project."]
                .exists
        )
        
        XCTAssert(
            XCUIApplication()
                .collectionViews
                .cells
                .staticTexts["1000"]
                .exists
        )
        
        XCTAssert(
            XCUIApplication()
                .collectionViews
                .cells
                .staticTexts["1234"]
                .exists
        )
        
        XCTAssert(
            XCUIApplication()
                .collectionViews
                .cells
                .staticTexts["1234"]
                .exists
        )
        
        app.collectionViews
            .element
            .swipeUp()
        
        XCTAssert(
            XCUIApplication()
                .collectionViews
                .cells
                .staticTexts["ThirdRepo"]
                .exists
        )
        
    }
    
    func testShowingDetail() {
        router["/search/repositories"] = DelayResponse(JSONResponse(handler: { [unowned self] _ -> Any in
            return self.mockReposData
        }), delay: .delay(seconds: 0.0))
        
        router["/repos/dev/JavaRepo/pulls"] = DelayResponse(JSONResponse(handler: { [unowned self] _ -> Any in
            return self.mockPullsData
        }), delay: .delay(seconds: 0.0))
        
        app.launch()
        
        app.collectionViews.cells.staticTexts["JavaRepo"].tap()
        
        XCTAssert(app.navigationBars.staticTexts["JavaRepo"].exists)
        XCTAssert(app.tables.cells.staticTexts["Fixing something wrong"].exists)
        XCTAssert(app.tables.cells.staticTexts["It wasn't working, so I had to fix it."].exists)
        
        if let language = Locale.preferredLanguages.first {
            if language.hasPrefix("en") {
                XCTAssert(
                    app.tables.cells.staticTexts["03/02/17"].exists ||
                    app.tables.cells.staticTexts["3/2/17"].exists
                )
            } else if language.hasPrefix("pt") {
                XCTAssert(app.tables.cells.staticTexts["02/03/17"].exists)
            }
        }
    }
    
    let mockReposData: [String: Any] = [
        "items": [
            ["id": 1,
             "name": "JavaRepo",
             "description": "A very creative project.",
             "stargazers_count": 1000,
             "forks": 1234,
             "owner": ["id": 12,
                       "login": "dev",
                       "avatar_url": "-"]],
            ["id": 2,
             "name": "SecondRepo",
             "description": "Another very creative project.",
             "stargazers_count": 999,
             "forks": 1239],
            ["id": 3,
             "name": "ThirdRepo",
             "description": "Yet another very creative project.",
             "stargazers_count": 998,
             "forks": 1922]
        ]
    ]
    
    let mockPullsData: [[String: Any]] = [
        [
            "id": 1,
            "html_url": "https://apple.com",
            "title": "Fixing something wrong",
            "body": "It wasn't working, so I had to fix it.",
            "created_at": "2017-03-02T03:24:55Z",
            "state": "open",
            "user":["id": 12,
                    "login": "dev",
                    "avatar_url": "-"]
        ]
    ]
    
}
