//
//  UITestBase.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 23/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import XCTest

import Embassy
import EnvoyAmbassador

class UITestBase: XCTestCase {
    
    let port = 8088
    var router: Router!
    var eventLoop: SelectorEventLoop!
    var server: HTTPServer!
    var app: XCUIApplication!
    
    var eventCondition: NSCondition!
    var eventLoopThread: Thread!
    
    override func setUp() {
        super.setUp()
        setupWebApp()
        setupApp()
    }
    
    private func setupWebApp() {
        eventLoop = try! SelectorEventLoop(selector: try! KqueueSelector())
        router = Router()
        server = DefaultHTTPServer(eventLoop: eventLoop, port: port, app: router.app)
        
        try! server.start()
        
        eventCondition = NSCondition()
        eventLoopThread = Thread(target: self, selector: #selector(UITestBase.runEventLoop), object: nil)
        eventLoopThread.start()
    }
    
    private func setupApp() {
        app = XCUIApplication()
        app.launchEnvironment["webserviceURL"] = "http://[::1]:8088"
    }
    
    override func tearDown() {
        super.tearDown()
        app.terminate()
        server.stopAndWait()
        eventLoop.stop()
    }
    
    @objc func runEventLoop() {
        eventLoop.runForever()
        eventCondition.lock()
        eventCondition.signal()
        eventCondition.unlock()
    }
    
}
