//
//  RepositoryViewControllerTests.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 23/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import XCTest
@testable import ChallengeConcrete

extension RepositoryViewController: StoryboardRetrievable {}

class MockRepositoryViewModel: RepositoryViewModelType {
    
    var requestCalled = false
    var errorToReturn: Error?
    var openClosed = ""
    var pullRequests = [PullRequest]()
    
    func requestPulls(completion: PullRequestsCompletion?) {
        requestCalled = true
        completion?(errorToReturn)
    }
}

class MockRefreshControl: UIRefreshControl {
    
    var beginCalled = false
    var endCalled = false
    
    override func beginRefreshing() {
        beginCalled = true
    }
    
    override func endRefreshing() {
        endCalled = true
    }
    
}

fileprivate class MockTableView: UITableView {
    
    var reloadDataCalled = false
    
    override func reloadData() {
        reloadDataCalled = true
    }
    
    override func dequeueReusableCell(withIdentifier identifier: String,
                                      for indexPath: IndexPath) -> UITableViewCell {
        return MockPullCell()
    }
    
}

fileprivate class MockPullCell: PullRequestTableViewCell {}

class RepositoryViewControllerTests: XCTestCase {
    
    var sut: RepositoryViewController!
    
    override func setUp() {
        sut = RepositoryViewController.getViewController(loading: true)
    }
    
    func testLoading() {
        XCTAssertNotNil(sut.pullRequestsTableView.dataSource)
        XCTAssertNotNil(sut.pullRequestsTableView.delegate)
        XCTAssertNotNil(sut.messagePresenter)
        XCTAssertNotNil(sut.refreshControl)
    }
    
    func testSettingRepo() {
        let repo = Repo(number: 12)
        sut.repo = repo
        
        sut.viewDidLoad()
        
        XCTAssertEqual(sut.title, "repo #12")
    }
    
    func testCallingRequest() {
        let viewModel = MockRepositoryViewModel()
        sut.viewModel = viewModel
        let repo = Repo(number: 5)
        sut.repo = repo
        let refresh = MockRefreshControl()
        sut.refreshControl = refresh
        
        sut.viewWillAppear(false)
        
        XCTAssert(viewModel.requestCalled)
        XCTAssert(refresh.beginCalled)
        XCTAssert(refresh.endCalled)
    }
    
    func testReloading() {
        let viewModel = MockRepositoryViewModel()
        sut.viewModel = viewModel
        let tableView = MockTableView()
        sut.pullRequestsTableView = tableView
        
        sut.handleRefresh(MockRefreshControl())
        
        XCTAssert(tableView.reloadDataCalled)
    }
    
    func testDataSource() {
        let viewModel = MockRepositoryViewModel()
        let pr1 = PullRequest(id: 1,
                              htmlURL: "url1",
                              title: "title1",
                              description: "description1",
                              dateStr: "2017-03-02T03:24:55Z",
                              stateStr: "open")
        let pr2 = PullRequest(id: 2,
                              htmlURL: "url2",
                              title: "title2",
                              description: "description2",
                              dateStr: "2017-03-02T03:24:55Z",
                              stateStr: "open")
        viewModel.pullRequests = [pr1, pr2]
        sut.viewModel = viewModel
        let tableView = MockTableView()
        sut.pullRequestsTableView = tableView
        
        let rows = sut.tableView(sut.pullRequestsTableView,
                                 numberOfRowsInSection: 0)
        let cell = sut.tableView(sut.pullRequestsTableView,
                                 cellForRowAt: IndexPath(row: 1, section: 0))
        
        let pullCell = cell as? PullRequestTableViewCell
        
        XCTAssertEqual(rows, 2)
        XCTAssertNotNil(pullCell)
        XCTAssertEqual(pullCell?.pullRequest, pr2)
    }
}
