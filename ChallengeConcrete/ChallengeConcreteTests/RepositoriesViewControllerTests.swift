//
//  RepositoriesViewControllerTests.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 22/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import XCTest
@testable import ChallengeConcrete

extension RepositoriesViewController: StoryboardRetrievable {}

fileprivate class MockRepositoriesViewModel: RepositoriesViewModelType {
    
    var reposToReturn: [Repo]?
    var errorToReturn: Error?
    var requestCalled = false
    var passedPage: Int?
    
    var repos: [Repo] {
        return reposToReturn ?? []
    }
    
    func request(page: Int, completion: RepositoriesCompletion?) {
        requestCalled = true
        passedPage = page
        completion?(reposToReturn, errorToReturn)
    }
}

fileprivate class MockCollectionView: UICollectionView {
    
    var reloadDataCalled = false
    
    init() {
        super.init(frame: CGRect.zero,
                   collectionViewLayout: UICollectionViewFlowLayout())
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func reloadData() {
        reloadDataCalled = true
    }
    
    override func dequeueReusableCell(withReuseIdentifier identifier: String,
                                      for indexPath: IndexPath) -> UICollectionViewCell {
        return MockCollectionViewCell()
    }
    
}

fileprivate class MockSegueController: MainSegueControllerType {
    
    var showCalled = false
    var passedViewController: UIViewController?
    var passedSender: Any?
    
    func showRepositoryScreen(from viewController: UIViewController, sender: Any?) {
        showCalled = true
        passedViewController = viewController
        passedSender = sender
    }
    
}

class MockCollectionViewCell: RepoCollectionViewCell {}

class RepositoriesViewControllerTests: XCTestCase {
    
    var sut: RepositoriesViewController!
    
    override func setUp() {
        sut = RepositoriesViewController.getViewController(loading: true)
    }
    
    func testLoading() {
        XCTAssertEqual(sut.currentPage, 0)
        XCTAssertNotNil(sut.viewModel)
        XCTAssertNotNil(sut.segueController)
        XCTAssertNotNil(sut.reposCollectionView.dataSource)
        XCTAssertNotNil(sut.reposCollectionView.delegate)
        XCTAssertNotNil(sut.reposCollectionView.collectionViewLayout)
    }
    
    func testCallingViewModel() {
        let viewModel = MockRepositoriesViewModel()
        sut.viewModel = viewModel
        
        sut.viewWillAppear(false)
        
        XCTAssert(viewModel.requestCalled)
    }
    
    func testRequestCompletionSuccess() {
        let viewModel = MockRepositoriesViewModel()
        let collectionView = MockCollectionView()
        sut.viewModel = viewModel
        sut.reposCollectionView = collectionView
        
        sut.viewWillAppear(false)
        
        XCTAssert(collectionView.reloadDataCalled)
    }
    
    func testCollectionDataSource() {
        let viewModel = MockRepositoriesViewModel()
        let repos = getRepos(10)
        viewModel.reposToReturn = repos
        let collectionView = MockCollectionView()
        sut.viewModel = viewModel
        sut.reposCollectionView = collectionView
        
        sut.viewWillAppear(false)
    
        let items = sut.collectionView(collectionView,
                                       numberOfItemsInSection: 0)
        let cell = sut.collectionView(collectionView,
                                      cellForItemAt: IndexPath(item: 5, section: 0))
        
        XCTAssertEqual(items, 10)
        XCTAssertEqual((cell as? MockCollectionViewCell)?.repo?.name,
                       "repo #5")

    }
    
    func testCallingSegueController() {
        let viewModel = MockRepositoriesViewModel()
        let repos = getRepos(10)
        viewModel.reposToReturn = repos
        let collectionView = MockCollectionView()
        let controller = MockSegueController()
        sut.viewModel = viewModel
        sut.reposCollectionView = collectionView
        sut.segueController = controller
        
        sut.collectionView(collectionView, didSelectItemAt: IndexPath(item: 0, section: 0))
        
        XCTAssert(controller.showCalled)
        XCTAssertEqual(controller.passedViewController, sut)
        XCTAssertEqual((controller.passedSender as? Repo)?.id, repos[0].id)
    }
    
    func testPrepareForSegue() {
        let vc = RepositoryViewController()
        let segue = UIStoryboardSegue(identifier: "ShowRepo",
                                      source: sut,
                                      destination: vc)
        let repo = Repo(number: 5)

        sut.prepare(for: segue, sender: repo)
        
        XCTAssertEqual(vc.repo, repo)
    }
    
    func getRepos(_ number: Int = 0) -> [Repo] {
        var repos = [Repo]()
        
        if number > 0 {
            for i in 0..<number {
                let repo = Repo(number: i)
                repos.append(repo)
                
            }
        }
        
        return repos
    }
}

extension Repo {
    
    init(number: Int) {
        id = number
        name = "repo #\(number)"
        description = "Another repo"
        stars = 5
        forks = 5
        creator = nil
    }
    
}
