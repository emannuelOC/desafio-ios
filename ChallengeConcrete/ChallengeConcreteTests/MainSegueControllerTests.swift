//
//  MainSegueControllerTests.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 23/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import XCTest
@testable import ChallengeConcrete

fileprivate class MockViewController: UIViewController {
    
    var performCalled = false
    var passedIdentifier: String?
    var passedSender: Any?
    
    override func performSegue(withIdentifier identifier: String, sender: Any?) {
        performCalled = true
        passedIdentifier = identifier
        passedSender = sender
    }
    
}

class MainSegueControllerTests: XCTestCase {
    
    var sut: MainSegueController!
    
    override func setUp() {
        sut = MainSegueController()
    }
    
    func testShowRepository() {
        let vc = MockViewController()
        
        sut.showRepositoryScreen(from: vc, sender: 13)
        
        XCTAssert(vc.performCalled)
        XCTAssertEqual(vc.passedIdentifier, "ShowRepo")
        XCTAssertEqual((vc.passedSender as? Int), 13)
    }
    
}
