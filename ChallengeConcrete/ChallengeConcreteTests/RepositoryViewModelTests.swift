//
//  RepositoryViewModelTests.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 24/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import XCTest
import Moya
@testable import ChallengeConcrete

class RepositoryViewModelTests: XCTestCase {
    
    var sut: RepositoryViewModel!
    
    override func setUp() {
        var repo = Repo(number: 12)
        let user = User(id: 1, username: "user", avatarURL: "url")
        repo.creator = user
        sut = RepositoryViewModel(repo: repo)
    }
    
    func testNothing() {
        
        let provider = MoyaProvider<GitHubService>(stubClosure: MoyaProvider.immediatelyStub)
        sut.provider = provider
        
        sut.requestPulls(completion: nil)
        
        XCTAssertEqual(self.sut.pullRequests.count, 1)
        XCTAssertEqual(self.sut.pullRequests[0].id, 1)
        XCTAssertEqual(self.sut.pullRequests[0].title, "my title")
        XCTAssertEqual(self.sut.pullRequests[0].htmlURL, "any_url")
        XCTAssertEqual(self.sut.openClosed, "1 open / 0 closed")
        
    }
    
}

extension User {
    
    init(id: Int, username: String, avatarURL: String) {
        self.id = id
        self.username = username
        self.avatarURL = avatarURL
    }
    
}
