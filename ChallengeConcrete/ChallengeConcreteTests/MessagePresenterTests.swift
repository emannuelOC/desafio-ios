//
//  MessagePresenterTests.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 23/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import XCTest
@testable import ChallengeConcrete

fileprivate class MockViewController: UIViewController {
    
    var presentCalled = false
    var passedViewController: UIViewController?
    
    override func present(_ viewControllerToPresent: UIViewController,
                          animated flag: Bool,
                          completion: (() -> Void)? = nil) {
        presentCalled = true
        passedViewController = viewControllerToPresent
    }
    
}

class MessagePresenterTests: XCTestCase {
    
    var sut: MessagePresenter!
    
    override func setUp() {
        sut = MessagePresenter()
    }
    
    func testPresenting() {
        let vc = MockViewController()
        
        sut.present(message: "My message",
                    title: "My title",
                    on: vc,
                    completion: nil)
        
        let alert = vc.passedViewController as? UIAlertController
        XCTAssertNotNil(alert)
        XCTAssertEqual(alert?.title, "My title")
        XCTAssertEqual(alert?.message, "My message")
    }
    
}
