//
//  PullRequestTableViewCellTests.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 23/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import XCTest
@testable import ChallengeConcrete

fileprivate let defaultFrame = CGRect(x: 0, y: 0, width: 100, height: 100)

class PullRequestTableViewCellTests: XCTestCase {
    
    var sut: PullRequestTableViewCell!
    
    let titleLabel = UILabel(frame: defaultFrame)
    let descLabel = UILabel(frame: defaultFrame)
    let creatorLabel = UILabel(frame: defaultFrame)
    let dateLabel = UILabel(frame: defaultFrame)
    let stateLabel = UILabel(frame: defaultFrame)
    
    override func setUp() {
        sut = PullRequestTableViewCell(style: .default,
                                       reuseIdentifier: "PullRequestCell")
        
        _ = sut.contentView
        
        sut.titleLabel = titleLabel
        sut.descriptionLabel = descLabel
        sut.creatorUsernameLabel = creatorLabel
        sut.dateLabel = dateLabel
        sut.stateLabel = stateLabel
    
    }
    
    func testUpdateUI() {
        let pullRequest = PullRequest(id: 7,
                                      htmlURL: "my url",
                                      title: "its title",
                                      description: "the PR",
                                      dateStr: "2017-03-02T03:24:55Z",
                                      stateStr: "open")
        
        sut.pullRequest = pullRequest
        
        XCTAssertEqual(sut.titleLabel.text, "its title")
        XCTAssertEqual(sut.descriptionLabel.text, "the PR")
        XCTAssertEqual(sut.stateLabel.text, "Open")
        
        if let language = Locale.preferredLanguages.first {
            if language.hasPrefix("en") {
                XCTAssert(sut.dateLabel.text == "03/02/17" || sut.dateLabel.text == "3/2/17")
            } else if language.hasPrefix("pt") {
                XCTAssertEqual(sut.dateLabel.text, "02/03/17")
            }
        }
    }
    
}

extension PullRequest {
    
    init(id: Int,
         htmlURL: String,
         title: String,
         description: String,
         dateStr: String,
         stateStr: String,
         creator: User? = nil) {
        self.id = id
        self.htmlURL = htmlURL
        self.title = title
        self.description = description
        self.dateStr = dateStr
        self.stateStr = stateStr
        self.creator = creator
    }
    
}
