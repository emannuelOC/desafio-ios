//
//  StoryboardRetrievable.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 22/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import UIKit

protocol StoryboardRetrievable {
    
    var view: UIView! { get } 
    static var storyboardIdentifier: String { get }
    static func getViewController(loading: Bool) -> Self
    
}

extension StoryboardRetrievable {
    
    static var storyboardIdentifier: String { return String(describing: Self.self) }
    
}

extension StoryboardRetrievable {
    
    static func getViewController(loading: Bool) -> Self {
        guard let vc = UIStoryboard(name: "Main", bundle: nil)
            .instantiateViewController(withIdentifier: storyboardIdentifier)
            as? Self else {
                fatalError("The storyboard identifier should be the name of the class.")
        }
        if loading {
            _ = vc.view
        }
        return vc
    }
    
}
