//
//  RepositoriesViewModelTests.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 24/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import XCTest
import Moya
@testable import ChallengeConcrete

class RepositoriesViewModelTests: XCTestCase {
    
    var sut: RepositoriesViewModel!
    
    override func setUp() {
        sut = RepositoriesViewModel()
    }
    
    func testRequest() {
        let provider = MoyaProvider<GitHubService>(stubClosure: MoyaProvider.immediatelyStub)
        sut.provider = provider
        
        sut.request(page: 0, completion: nil)
        
        XCTAssertEqual(sut.repos.count, 2)
        
        XCTAssertEqual(sut.repos[0].name, "JavaRepo2")
        XCTAssertEqual(sut.repos[0].forks, 12)
        XCTAssertEqual(sut.repos[0].creator?.username, "dev2")
        XCTAssertEqual(sut.repos[0].stars, 13)
        
        XCTAssertEqual(sut.repos[1].name, "JavaRepo")
        XCTAssertEqual(sut.repos[1].forks, 12)
        XCTAssertEqual(sut.repos[1].creator?.username, "dev")
        XCTAssertEqual(sut.repos[1].stars, 12)
    }
    
}
