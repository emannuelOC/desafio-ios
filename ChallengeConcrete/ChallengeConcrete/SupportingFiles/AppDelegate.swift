//
//  AppDelegate.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 17/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

}

