//
//  MainSegueController.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 23/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import UIKit

protocol MainSegueControllerType {
    
    func showRepositoryScreen(from viewController: UIViewController,
                              sender: Any?)
    
}

struct MainSegueController: MainSegueControllerType {
    
    func showRepositoryScreen(from viewController: UIViewController,
                              sender: Any?) {
        viewController.performSegue(withIdentifier: "ShowRepo", sender: sender)
    }
    
}
