//
//  RepositoriesViewModel.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 17/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import Foundation
import Moya
import Moya_ObjectMapper

class RepositoriesViewModel: RepositoriesViewModelType {
    
    var repos = [Repo]()
    
    var provider =  MoyaProvider<GitHubService>() 
        
    func request(page: Int, completion: RepositoriesCompletion?) {
        provider.request(.repositories(page: page)) { (result) in
            switch result {
            case let .success(response):
                do {
                    let newRepos = try response
                        .removeAPIWrappers()
                        .mapArray(Repo.self)
                        .sorted(by: { (r1, r2) -> Bool in
                            r1.stars > r2.stars
                        })
                    self.repos.append(contentsOf: newRepos)
                    completion?(self.repos, nil)
                } catch {
                    completion?(nil, error)
                }
            case let .failure(error):
                completion?(nil, error)
            }
        }
    }
    
    
}
