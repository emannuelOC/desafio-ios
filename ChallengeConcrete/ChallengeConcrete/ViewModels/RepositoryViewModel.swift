//
//  RepositoryViewModel.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 23/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import Foundation
import Moya

class RepositoryViewModel: RepositoryViewModelType {
    
    var provider = MoyaProvider<GitHubService>()
    
    var pullRequests = [PullRequest]() {
        didSet {
            let open = pullRequests.filter { $0.state == .open }.count
            let closed = pullRequests.count - open
            openClosed = "\(open) open / \(closed) closed"
        }
    }
    
    var openClosed = ""
    
    var repository: Repo
    
    init(repo: Repo) {
        self.repository = repo
    }
    
    func requestPulls(completion: PullRequestsCompletion?) {
        guard let creator = self.repository.creator?.username else { return }
        let repository = self.repository.name
        provider.request(.pullRequests(creator: creator, repository: repository)) {
            [weak self] (result) in
            switch result {
            case let .success(response):
                do {
                    let pullRequests = try response
                        .mapArray(PullRequest.self)
                        .sorted(by: { (p1, p2) -> Bool in
                            guard let date1 = p1.date,
                                let date2 = p2.date else {
                                    return true
                            }
                            return date1 > date2
                        })
                    self?.pullRequests = pullRequests
                    completion?(nil)
                } catch {
                    completion?(error)
                }
            case let .failure(error):
                print(error)
            }
        }
    }
    
}
