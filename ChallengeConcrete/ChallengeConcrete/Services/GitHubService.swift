//
//  GitHubService.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 17/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import Foundation
import Moya

enum GitHubService {
    
    case repositories(page: Int)
    case pullRequests(creator: String, repository: String)
    
}

extension Response {
    func removeAPIWrappers() -> Response {
        guard let json = try? self.mapJSON() as? [String: Any],
            let items = json?["items"] as? [[String: Any]],
            let newData = try? JSONSerialization.data(withJSONObject: items,
                                                      options: .prettyPrinted) else {
                return self
        }
        
        let newResponse = Response(statusCode: self.statusCode,
                                   data: newData,
                                   response: self.response)
        return newResponse
    }
}

extension GitHubService: TargetType {
    
    var baseURL: URL {
        let urlString = ProcessInfo.processInfo.environment["webserviceURL"] ?? "https://api.github.com"
        return URL(string: urlString)!
    }
    
    var path: String {
        switch self {
        case .repositories:
            return "/search/repositories"
        case let .pullRequests(creator: creator, repository: repo):
            return "/repos/\(creator)/\(repo)/pulls"
        }
    }
    
    var method: Moya.Method { return .get }
    
    var parameters: [String : Any]? {
        switch self {
        case let .repositories(page):
            return ["q": "language:Java", "sort": "stars", "page": page]
        default:
            return ["state": "all"]
        }
    }
    
    var parameterEncoding: ParameterEncoding { return URLEncoding.default }
    
    var sampleData: Data {
        switch self {
        case .repositories:
            return reposString.data(using: .utf8)!
        case .pullRequests:
            return pullRequestsString.data(using: .utf8)!
        }
    }
    
    var reposString: String {
        let repos = ""
            + "{ \"items\":"
            + "[ { \"id\": 1, \"name\": \"JavaRepo\","
            + "\"description\": \"my description\","
            + "\"stargazers_count\": 12, \"forks\": 12,"
            + "\"owner\": { \"id\": 1, \"login\": \"dev\", \"avatar_url\": \"-\"}},"
            + "{ \"id\": 2, \"name\": \"JavaRepo2\","
            + "\"description\": \"my description 2\","
            + "\"stargazers_count\": 13, \"forks\": 12,"
            + "\"owner\": { \"id\": 2, \"login\": \"dev2\", \"avatar_url\": \"-\"}} ] }"
        return repos
    }
    
    var pullRequestsString: String {
        let pullsString = ""
            + "[\n"
            + "{\"id\": 1, \"html_url\": \"any_url\","
            + "\"title\": \"my title\", \"body\": \"my body\","
            + "\"created_at\": \"2017-03-02T03:24:55Z\","
            + "\"state\": \"open\","
            + "\"user\": {\"id\": 12, \"login\": \"dev\", \"avatar_url\": \"-\"}}]"
        return pullsString
    }
    
    var task: Task { return .request }
}
