//
//  UIColorExtension.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 22/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import UIKit

extension UIColor {
    
    static let concreteBlue = #colorLiteral(red: 0.25006561, green: 0.4542558107, blue: 1, alpha: 1)
    static let concreteLightBlue = #colorLiteral(red: 0.3043038218, green: 0.7633917512, blue: 1, alpha: 1)
    static let concretePurple = #colorLiteral(red: 0.4941176471, green: 0.2117647059, blue: 1, alpha: 1)
    static let concreteGreen = #colorLiteral(red: 0.2929941363, green: 1, blue: 0.8162779324, alpha: 1)
//    static let concreteDarkGray = #colorLiteral(red: 0.2806734604, green: 0.289923235, blue: 0.427268401, alpha: 1)
//    static let concreteGray = #colorLiteral(red: 0.3973151423, green: 0.3992624397, blue: 0.5, alpha: 1)
    static let concreteLightGray = #colorLiteral(red: 0.7447537877, green: 0.7480260108, blue: 0.8687539657, alpha: 1)
    static let concretePink = #colorLiteral(red: 0.7765336627, green: 0.09227698203, blue: 0.4109183191, alpha: 1)
    
}
