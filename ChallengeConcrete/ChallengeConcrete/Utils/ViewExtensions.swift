//
//  ViewExtensions.swift
//  Braincoms
//
//  Created by Emannuel Carvalho on 17/06/17.
//  Copyright © 2017 OC. All rights reserved.
//

import UIKit

extension UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
        }
    }
    
    @IBInspectable var borderColor: UIColor {
        get {
            if let color = layer.borderColor {
                return UIColor(cgColor: color)
            } else {
                return #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
            }
        }
        set {
            layer.borderColor = newValue.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat {
        get {
            return layer.borderWidth
        }
        set {
            layer.borderWidth = newValue
        }
    }
    
}

@IBDesignable class OCView: UIView {}
@IBDesignable class OCButton: UIButton {}
@IBDesignable class OCImageView: UIImageView {
    
    @IBInspectable override var cornerRadius: CGFloat {
        get {
            return layer.cornerRadius
        }
        set {
            layer.cornerRadius = newValue
            layer.masksToBounds = newValue > 0
        }
    }
    
}
