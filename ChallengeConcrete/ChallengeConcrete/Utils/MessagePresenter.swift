//
//  MessagePresenter.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 23/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import UIKit

protocol MessagePresenterType {
    
    func present(message: String?,
                 title: String?,
                 on viewController: UIViewController,
                 completion: (() -> Void)?)
    
}

struct MessagePresenter: MessagePresenterType {
    
    func present(message: String?,
                 title: String?,
                 on viewController: UIViewController,
                 completion: (() -> Void)?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(action)
        viewController.present(alert, animated: true, completion: completion)
    }
    
}
