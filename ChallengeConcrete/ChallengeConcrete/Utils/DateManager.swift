//
//  DateManager.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 23/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import Foundation

class DateManager {
    
    static let shared = DateManager()
    
    fileprivate var shortDateFormatter = DateFormatter()
    fileprivate var isoDateFormatter = DateFormatter()
    
    init() {
        shortDateFormatter.calendar = Calendar.current
        isoDateFormatter.calendar = Calendar.current
        shortDateFormatter.dateStyle = .short
        isoDateFormatter.dateFormat = "YYYY-MM-dd'T'HH:mm:ssZ"
    }
    
    func shorString(from date: Date) -> String {
        return shortDateFormatter.string(from: date)
    }
    
    func date(from string: String) -> Date? {
        return isoDateFormatter.date(from: string)
    }
    
}
