//
//  ConcreteIndicatorView.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 22/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import UIKit

@IBDesignable class ConcreteIndicatorView: UIView {
    
    @IBInspectable var radius: CGFloat = 15.0
    
    var layer1: CAShapeLayer!
    var layer2: CAShapeLayer!
    var layer3: CAShapeLayer!
    var layer4: CAShapeLayer!
    
    override func layoutSubviews() {
        if ((layer.sublayers ?? []).count == 0) {
            createLayers()
        }
    }
    
    fileprivate func createLayers() {
        let endCircle = UIBezierPath(ovalIn: CGRect(x: layer.center.x - 12,
                                                    y: layer.center.y,
                                                    width: 24,
                                                    height: 24)).cgPath
        
        let endCircle2 = UIBezierPath(ovalIn: CGRect(x: layer.center.x - 18,
                                                    y: layer.center.y,
                                                    width: 36,
                                                    height: 36)).cgPath
        
        layer1 = CAShapeLayer()
        layer1.frame = self.bounds
        layer1?.path = getPath1().cgPath
        layer1?.strokeColor = UIColor.concreteBlue.cgColor
        let animation = CABasicAnimation(keyPath: "path")
        animation.toValue = endCircle
        animation.duration = 1.2 // duration is 1 sec
        animation.autoreverses = true
        animation.repeatCount = HUGE
        animation.isRemovedOnCompletion = false
        layer1?.add(animation, forKey: animation.keyPath)
        
        layer2 = CAShapeLayer()
        layer2.frame = self.bounds
        layer2?.path = getPath2().cgPath
        layer2?.strokeColor = UIColor.concreteLightBlue.cgColor
        let animation2 = CABasicAnimation(keyPath: "path")
        animation2.toValue = getPath4().cgPath
        animation2.duration = 0.7 // duration is 1 sec
        animation2.autoreverses = true
        animation2.repeatCount = HUGE
        animation2.isRemovedOnCompletion = false
        layer2?.add(animation2, forKey: animation.keyPath)
        
        layer3 = CAShapeLayer()
        layer3.frame = self.bounds
        layer3?.path = getPath3().cgPath
        layer3?.strokeColor = UIColor.concretePurple.cgColor
        let animation3 = CABasicAnimation(keyPath: "path")
        animation3.toValue = getPath1().cgPath
        animation3.duration = 1.5 // duration is 1 sec
        animation3.autoreverses = true
        animation3.repeatCount = HUGE
        animation3.isRemovedOnCompletion = false
        layer3?.add(animation3, forKey: animation.keyPath)
        
        layer4 = CAShapeLayer()
        layer4.frame = self.bounds
        layer4?.path = getPath4().cgPath
        layer4?.strokeColor = UIColor.concreteGreen.cgColor
        let animation4 = CABasicAnimation(keyPath: "path")
        animation4.toValue = endCircle2
        animation4.duration = 1.5 // duration is 1 sec
        animation4.autoreverses = true
        animation4.repeatCount = HUGE
        animation4.isRemovedOnCompletion = false
        layer4?.add(animation4, forKey: animation.keyPath)
        
        for l in [layer1, layer2, layer3, layer4] {
            l?.fillColor = UIColor.clear.cgColor
        }
        
        layer.addSublayer(layer1)
        layer.addSublayer(layer2)
        layer.addSublayer(layer3)
        layer.addSublayer(layer4)
    }
    
    func getPath1() -> UIBezierPath {
        let p1 = CGPoint(x: layer.center.x, y: center.y - 1*radius)
        let p2 = CGPoint(x: layer.center.x + 1*radius, y: center.y)
        let p3 = CGPoint(x: layer.center.x, y: center.y + 1*radius)
        let p4 = CGPoint(x: layer.center.x - 1*radius, y: center.y)
        
        let path1 = UIBezierPath()
        path1.move(to: p1)
        path1.addCurve(to: p2,
                       controlPoint1: p1 + (20, 1.5),
                       controlPoint2: p2 + (0, -1.5))
        path1.addCurve(to: p3,
                       controlPoint1: p2 + (-2, 17),
                       controlPoint2: p3 + (-2.5, -0.25))
        path1.addCurve(to: p4,
                       controlPoint1: p3 + (-10, 0),
                       controlPoint2: p4 + (-7.5, 5))
        path1.addCurve(to: p1,
                       controlPoint1: p4 + (10, -5),
                       controlPoint2: p1 + (-7.5, 0))
        return path1
    }
    
    func getPath2() -> UIBezierPath {
        let p1 = CGPoint(x: layer.center.x, y: center.y - 1*radius)
        let p2 = CGPoint(x: layer.center.x + 1*radius, y: center.y)
        let p3 = CGPoint(x: layer.center.x, y: center.y + 1*radius)
        let p4 = CGPoint(x: layer.center.x - 1*radius, y: center.y)
        
        let path2 = UIBezierPath()
        // path2
        path2.move(to: p1 + (0, -5))
        path2.addCurve(to: p2 + (5, 0),
                       controlPoint1: p1 + (20, -5),
                       controlPoint2: p2 + (5, 0))
        path2.addCurve(to: p3 + (0, 2.5),
                       controlPoint1: p2 + (5, 5),
                       controlPoint2: p3 + (12.5, 2.5))
        path2.addCurve(to: p4,
                       controlPoint1: p3 + (-12.5, 2.5),
                       controlPoint2: p4 + (-7.5, 5))
        path2.addCurve(to: p1 + (0, -5),
                       controlPoint1: p4 + (12.5, -2.5),
                       controlPoint2: p1 + (-7.5, -5))
        return path2
    }
    
    func getPath3() -> UIBezierPath {
        let p1 = CGPoint(x: layer.center.x, y: center.y - 1*radius)
        let p2 = CGPoint(x: layer.center.x + 1*radius, y: center.y)
        let p3 = CGPoint(x: layer.center.x, y: center.y + 1*radius)
        let p4 = CGPoint(x: layer.center.x - 1*radius, y: center.y)
        
        let path3 = UIBezierPath()
        
        // path3
        path3.move(to: p1 + (0, -10))
        path3.addCurve(to: p2 + (10, 0),
                       controlPoint1: p1 + (20, -10),
                       controlPoint2: p2 + (10, -5))
        path3.addCurve(to: p3 + (0, 5),
                       controlPoint1: p2 + (10, 6),
                       controlPoint2: p3 + (12.5, 5))
        path3.addCurve(to: p4 + (-5, 0),
                       controlPoint1: p3 + (-12.5, 2.5),
                       controlPoint2: p4 + (-7.5, 5))
        path3.addCurve(to: p1 + (0, -10),
                       controlPoint1: p4 + (-5, 0),
                       controlPoint2: p1 + (-12.5, -10))
        
        return path3
    }
    
    func getPath4() -> UIBezierPath {
        let p1 = CGPoint(x: layer.center.x, y: center.y - 1*radius)
        let p2 = CGPoint(x: layer.center.x + 1*radius, y: center.y)
        let p3 = CGPoint(x: layer.center.x, y: center.y + 1*radius)
        let p4 = CGPoint(x: layer.center.x - 1*radius, y: center.y)
        
        let path4 = UIBezierPath()
        
        // path4
        path4.move(to: p1 + (5, -12.5))
        path4.addCurve(to: p2 + (7.5, 0),
                       controlPoint1: p1 + (15, -10),
                       controlPoint2: p2 + (7.5, -7.5))
        path4.addCurve(to: p3 + (0, 10),
                       controlPoint1: p2 + (7.5, 11),
                       controlPoint2: p3 + (15, 10))
        path4.addCurve(to: p4 + (-7.5, 0),
                       controlPoint1: p3 + (-10, 11),
                       controlPoint2: p4 + (-15, 5))
        path4.addCurve(to: p1 + (5, -12.5),
                       controlPoint1: p4 + (11, -2.5),
                       controlPoint2: p1 + (-12.5, -15))
        
        return path4
        
    }

}

extension CALayer {
    
    var center: CGPoint {
        return CGPoint(x: frame.width/2, y: frame.height/2)
    }
    
}

func +(lhs: CGPoint, rhs: CGPoint) -> CGPoint {
    return CGPoint(x: lhs.x + rhs.x, y: lhs.y + rhs.y)
}

func +(lhs: CGPoint, rhs: (Double, Double)) -> CGPoint {
    return CGPoint(x: lhs.x + CGFloat(rhs.0), y: lhs.y + CGFloat(rhs.1))
}
