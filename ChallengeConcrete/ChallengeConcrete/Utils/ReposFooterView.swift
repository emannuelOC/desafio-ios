//
//  ReposFooterView.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 22/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import UIKit

class ReposFooterView: UICollectionReusableView {

    @IBOutlet weak var activityIndicator: UIActivityIndicatorView! {
        didSet {
            activityIndicator.startAnimating()
        }
    }
    
}
