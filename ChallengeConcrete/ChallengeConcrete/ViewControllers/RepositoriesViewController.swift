//
//  ViewController.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 17/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import UIKit

typealias RepositoriesCompletion = ([Repo]?, Error?) -> Void

protocol RepositoriesViewModelType {
    var repos: [Repo] { get }
    func request(page: Int, completion: RepositoriesCompletion?)
}

final class RepositoriesViewController: UIViewController {

    @IBOutlet weak var reposCollectionView: UICollectionView!
    
    var viewModel: RepositoriesViewModelType?
    var segueController: MainSegueControllerType?
    var messagePresenter: MessagePresenterType?
    
    var currentPage = 0

    fileprivate func getRepos() {
        viewModel?.request(page: currentPage, completion: {
            [weak self] (_, error) in
            if let error = error {
                self?.messagePresenter?.present(message: error.localizedDescription,
                                          title: "Error",
                                          on: self!,
                                          completion: nil)
            } else {
                self?.reposCollectionView.reloadData()
            }
        })
    }
    
}

extension RepositoriesViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = RepositoriesViewModel()
        segueController = MainSegueController()
        messagePresenter = MessagePresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        getRepos()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        reposCollectionView.reloadData()
    }
    
}

extension RepositoriesViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView,
                        numberOfItemsInSection section: Int) -> Int {
        return viewModel?.repos.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "RepoCell",
                                                      for: indexPath) as! RepoCollectionViewCell
        if let repo = viewModel?.repos[indexPath.item] {
            cell.repo = repo
        }
        return cell
    }
    
}

extension RepositoriesViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {
        if kind == UICollectionElementKindSectionFooter {
            let view = collectionView
                .dequeueReusableSupplementaryView(ofKind: kind,
                                                  withReuseIdentifier: "ReposFooter",
                                                  for: indexPath) as! ReposFooterView
            return view
        }
        return ReposFooterView()
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        willDisplaySupplementaryView view: UICollectionReusableView,
                        forElementKind elementKind: String,
                        at indexPath: IndexPath) {
        currentPage += 1
        getRepos()
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        didSelectItemAt indexPath: IndexPath) {
        if let repo = viewModel?.repos[indexPath.item] {
            segueController?.showRepositoryScreen(from: self, sender: repo)
        }
    }
    
}


extension RepositoriesViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width = self.view.bounds.width
        if width > 400 {
            width /= 2
        }
        
        return CGSize(width: width - 42, height: 200)
    }
    
}

extension RepositoriesViewController {
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? RepositoryViewController {
            vc.repo = sender as? Repo
        }
    }
    
}

