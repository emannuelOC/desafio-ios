//
//  RepositoryViewController.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 22/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import UIKit
import SafariServices

typealias PullRequestsCompletion = (Error?) -> Void

protocol RepositoryViewModelType {
    func requestPulls(completion: PullRequestsCompletion?)
    var openClosed: String { get }
    var pullRequests: [PullRequest] { get }    
}

final class RepositoryViewController: UIViewController {

    @IBOutlet weak var openClosedLabel: UILabel!
    @IBOutlet weak var pullRequestsTableView: UITableView!
    
    fileprivate var customView: UIView!
    
    var repo: Repo? {
        didSet {
            title = repo?.name ?? ""
        }
    }
    
    lazy var refreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self,
                                 action: #selector(RepositoryViewController.handleRefresh(_:)),
                                 for: .valueChanged)

        return refreshControl
    }()
    
    var viewModel: RepositoryViewModelType?
    
    var messagePresenter: MessagePresenterType?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        pullRequestsTableView.estimatedRowHeight = 150
        pullRequestsTableView.addSubview(refreshControl)
        loadCustomRefresh()
        messagePresenter = MessagePresenter()
        if let repo = repo {
            viewModel = RepositoryViewModel(repo: repo)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        refreshControl.beginRefreshing()
        requestPulls()
    }
    
}

extension RepositoryViewController {
    
    func handleRefresh(_ refreshControl: UIRefreshControl) {
        requestPulls()
    }

    fileprivate func loadCustomRefresh() {
        let refreshContents = Bundle.main.loadNibNamed("ConcreteRefresh",
                                                       owner: self,
                                                       options: nil)
        customView = refreshContents?[0] as? UIView
        customView.frame = refreshControl.bounds
        refreshControl.addSubview(customView)
    }
    
    fileprivate func requestPulls() {
        viewModel?.requestPulls {
            self.refreshControl.endRefreshing()
            if let error = $0 {
                print(error)
            } else {
                self.pullRequestsTableView.reloadData()
                self.openClosedLabel.text = self.viewModel?.openClosed
            }
        }
    }

}

extension RepositoryViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView,
                   numberOfRowsInSection section: Int) -> Int {
        return viewModel?.pullRequests.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "PullRequestCell",
                                                 for: indexPath)
            as! PullRequestTableViewCell
        
        if let pull = viewModel?.pullRequests[indexPath.row] {
            cell.pullRequest = pull
        }
        
        return cell
    }
    
}

extension RepositoryViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView,
                   heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let pull = viewModel?.pullRequests[indexPath.row] {
            presentPullRequest(with: pull.htmlURL)
        }
    }
}

extension RepositoryViewController: SFSafariViewControllerDelegate {

    func presentPullRequest(with url: String) {
        guard let url = URL(string: url) else { return }
        if #available(iOS 9.0, *) {
            let safariViewController = SFSafariViewController(url: url,
                                                              entersReaderIfAvailable: true)
            safariViewController.view.tintColor = UIColor.concretePurple
            safariViewController.delegate = self
            present(safariViewController, animated: true, completion: nil)
        } else {
            UIApplication.shared.openURL(url)
        }
    }
    
    @available(iOS 9.0, *)
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        controller.dismiss(animated: true, completion: nil)
    }
    
}
