//
//  User.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 23/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import Foundation
import ObjectMapper

struct User: Mappable {
    
    var id = 0
    var username = ""
    var avatarURL = ""
    
    init?(map: Map) { }
    
    mutating func mapping(map: Map) {
        
        id <- map["id"]
        username <- map["login"]
        avatarURL <- map["avatar_url"]
    }
}

extension User: Equatable {}

func ==(lhs: User, rhs: User) -> Bool {
    return lhs.id == rhs.id
        && lhs.username == rhs.username
        && lhs.avatarURL == rhs.avatarURL
}
