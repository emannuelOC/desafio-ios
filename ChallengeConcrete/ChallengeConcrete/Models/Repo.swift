//
//  Repo.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 17/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import Foundation
import ObjectMapper

struct Repo {
    
    var id = 0
    var name = ""
    var description = ""
    var stars = 0
    var forks = 0
    var creator: User?
    
}

extension Repo: Mappable {

    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        name <- map["name"]
        description <- map["description"]
        stars <- map["stargazers_count"]
        forks <- map["forks"]
        creator <- map["owner"]
    }
    
}

extension Repo: Equatable {}

func ==(lhs: Repo, rhs: Repo) -> Bool {
    return lhs.id == rhs.id
        && lhs.name == rhs.name
        && lhs.description == rhs.description
        && lhs.stars == rhs.stars
        && lhs.forks == rhs.forks
        && lhs.creator == rhs.creator
}
