//
//  PullRequest.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 23/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import Foundation
import ObjectMapper

enum PullRequestState {
    case open, closed
}

struct PullRequest: Mappable {
    
    var id = 0
    var htmlURL = ""
    var title = ""
    var description = ""
    
    var dateStr = ""
    var stateStr = ""
    
    var creator: User?
    
    var date: Date? {
        return DateManager.shared.date(from: dateStr)
    }
    
    var state: PullRequestState {
        switch stateStr {
        case "open":
            return .open
        default:
            return .closed
        }
    }
    
    init?(map: Map) {}
    
    mutating func mapping(map: Map) {
        id <- map["id"]
        htmlURL <- map["html_url"]
        title <- map["title"]
        description <- map["body"]
        dateStr <- map["created_at"]
        stateStr <- map["state"]
        creator <- map["user"]
    }
    
}

extension PullRequest: Equatable {}

func ==(lhs: PullRequest, rhs: PullRequest) -> Bool {
    return lhs.id == rhs.id
        && lhs.htmlURL == rhs.htmlURL
        && lhs.title == rhs.title
        && lhs.description == rhs.description
        && lhs.dateStr == rhs.dateStr
        && lhs.creator == rhs.creator
}

