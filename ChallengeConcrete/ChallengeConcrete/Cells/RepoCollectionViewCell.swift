//
//  RepoCollectionViewCell.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 17/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import UIKit
import Kingfisher

class RepoCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var repoNameLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var pictureImageView: UIImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var starsLabel: UILabel!
    @IBOutlet weak var forksLabel: UILabel!
    
    var repo: Repo? {
        didSet {
            updateUI()
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        if let view = contentView.subviews.first {
            view.layer.shadowOpacity = 1
            view.layer.shadowColor = UIColor.lightGray.cgColor
            view.layer.shadowRadius = 7
            view.layer.shadowOffset = CGSize(width: 0, height: 2)
            self.clipsToBounds = false
        }
    }
  
    func updateUI() {
        guard let repo = repo else { return }
        repoNameLabel?.text = repo.name
        descriptionLabel?.text = repo.description
        usernameLabel?.text = repo.creator?.username
        pictureImageView?.image = nil
        starsLabel?.text = "\(repo.stars)"
        forksLabel?.text = "\(repo.forks)"
        if let url = URL(string: repo.creator?.avatarURL ?? "") {
            pictureImageView.kf.setImage(with: url)
        }
    }
}
