//
//  PullRequestTableViewCell.swift
//  ChallengeConcrete
//
//  Created by Emannuel Carvalho on 22/06/17.
//  Copyright © 2017 oc. All rights reserved.
//

import UIKit
import Kingfisher

class PullRequestTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var userPictureImageView: OCImageView!
    @IBOutlet weak var creatorUsernameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    
    var pullRequest: PullRequest? {
        didSet {
            updateUI()
        }
    }
    
    func updateUI() {
        titleLabel?.text = pullRequest?.title
        descriptionLabel?.text = pullRequest?.description
        creatorUsernameLabel?.text = pullRequest?.creator?.username
        userPictureImageView?.image = nil
        if let date = pullRequest?.date {
            dateLabel?.text = DateManager.shared.shorString(from: date)
        }
        if let url = URL(string: pullRequest?.creator?.avatarURL ?? "") {
            userPictureImageView.kf.setImage(with: url)
        }
        if pullRequest?.state == .open {
            stateLabel?.text = "Open"
            stateLabel?.textColor = UIColor.concreteBlue
        } else {
            stateLabel?.text = "Closed"
            stateLabel?.textColor = UIColor.concretePink
        }
    }
    
    
}
